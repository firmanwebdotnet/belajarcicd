#!/bin/bash

DEPLOY_SERVER=dev.fhnid.com
SERVER_FOLDER="belajarCICD"

# Building React output
yarn install
yarn run build

echo "Deploying to ${DEPLOY_SERVER}"
scp -r build/ root@${DEPLOY_SERVER}:/var/www/html/${SERVER_FOLDER}/

echo "Finished copying the build files"
